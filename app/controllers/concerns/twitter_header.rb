# frozen_string_literal: true

# Concern for twitter API's header
module TwitterHeader
  extend ActiveSupport::Concern
  def header_data
    {
      'Content-Type' => 'application/json',
      'Authorization' => "OAuth oauth_consumer_key=\"#{@consumer_key}\",oauth_token=\"#{@access_token}\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"1684848631\",oauth_nonce=\"XoeSVXeTTEw\",oauth_version=\"1.0\",oauth_callback=\"http%3A%2F%2Flocalhost%3A3000%2Fauth%2Fcallback\",oauth_signature=\"toWaX92P5Du33CTzwjRKRZh6IRI%3D\"",
      'Cookie' => 'guest_id=v1%3A168482124747507436'
    }
  end
end