# frozen_string_literal: true

require 'faraday'
require 'oauth'
require 'json'

# Handles twitter tweets
class TwitterController < ApplicationController
  before_action :fetch_tokens, :base_url
  include TwitterHeader

  #API for tweet text
  def post_text
    url = "#{@base_url}/2/tweets"
    headers = header_data

    body = { "text": params[:text] }

    response = Faraday.post(url) do |request|
      request.headers = headers
      request.body = JSON.generate(body)
    end
    if response.success?
      render json: { message: 'Text posted successfully on Twitter.' }
    else
      render json: { error: 'Failed to post text on Twitter.' }, status: :internal_server_error
    end
  end

  #API for tweet image
  def upload_image
    tweet_text = params[:text]
    image_data = params[:image].read
    url = 'https://upload.twitter.com/1.1/media/upload.json'

    connection = Faraday.new(url: @base_url) do |faraday|
      faraday.request :oauth, consumer_key: @consumer_key, consumer_secret: @consumer_secret, token: @access_token,
                              token_secret: @access_token_secret
      faraday.request :multipart
      faraday.adapter Faraday.default_adapter
    end

    media_response = connection.post('https://upload.twitter.com/1.1/media/upload.json') do |req|
      req.headers['Content-Type'] = 'multipart/form-data'
      req.body = {
        media: Faraday::UploadIO.new(StringIO.new(image_data), 'image/jpeg')
      }
    end
    # Extract the media ID from the response
    media_id = JSON.parse(media_response.body)['media_id']

    credentials = {
      consumer_key: @consumer_key,
      consumer_secret: @consumer_secret,
      token: @access_token,
      token_secret: @access_token_secret
    }

    # Create the Faraday connection
    connection = Faraday.new(url: "#{@base_url}/1.1") do |faraday|
      faraday.request :oauth, credentials
      faraday.request :multipart
      faraday.adapter Faraday.default_adapter
    end

    tweet_url = "#{@base_url}/2/tweets"
    headers = header_data

    body = { "text": tweet_text, "media": { "media_ids": [media_id.to_s] } }

    response = Faraday.post(tweet_url) do |request|
      request.headers = headers
      request.body = JSON.generate(body)
    end
    # Handle the tweet response
    if response.success?
      render json: { message: 'Tweet posted successfully!' }
    else
      render json: { message: 'Tweet could not be posted.', response_body: response.body }
    end
  end

  #API for tweet video
  def upload_video
    tweet_text = params[:text]
    video_data = params[:video].read
    url = 'https://upload.twitter.com/1.1/media/upload.json'

    # Create the Faraday connection for media upload
    media_connection = Faraday.new(url: 'https://upload.twitter.com/1.1') do |faraday|
      faraday.request :oauth, consumer_key: @consumer_key, consumer_secret: @consumer_secret, token: @access_token,
                              token_secret: @access_token_secret
      faraday.request :multipart
      faraday.adapter Faraday.default_adapter
    end

    # Upload the video
    media_response = media_connection.post(url) do |req|
      req.headers['Content-Type'] = 'multipart/form-data'
      req.body = {
        media: Faraday::UploadIO.new(StringIO.new(video_data), 'video/mp4')
      }
    end

    # Extract the media ID from the response
    media_id = JSON.parse(media_response.body)['media_id']

    credentials = {
      consumer_key: @consumer_key,
      consumer_secret: @consumer_secret,
      token: @access_token,
      token_secret: @access_token_secret
    }

    # Create the Faraday connection
    connection = Faraday.new(url: "#{@base_url}/1.1") do |faraday|
      faraday.request :oauth, credentials
      faraday.request :multipart
      faraday.adapter Faraday.default_adapter
    end

    tweet_url = "#{@base_url}/2/tweets"
    headers = header_data

    body = { "text": tweet_text, "media": { "media_ids": [media_id.to_s] } }

    response = Faraday.post(tweet_url) do |request|
      request.headers = headers
      request.body = JSON.generate(body)
    end

    # Handle the tweet response
    if response.success?
      render json: { message: 'Tweet posted successfully!', response_body: response.body }
    else
      render json: { message: 'Tweet could not be posted.', response_body: response.body }
    end
  end

  #API for fetching comments using twitter v1 endpoint
  def fetch_comments
    # Set up Faraday connection
    headers = header_data

    tweet_id = params[:tweet_id]

    conn = Faraday.new(url: @base_url) do |faraday|
      faraday.request :url_encoded
      faraday.adapter Faraday.default_adapter
    end
    # Fetch the tweet using the provided tweet ID and headers
    response = conn.get("/1.1/statuses/show.json?id=#{tweet_id}") do |req|
      headers.each { |key, value| req.headers[key] = value }
    end
  
    if response.status == 200
      tweet = JSON.parse(response.body)
      conversation_id = tweet['in_reply_to_status_id'].to_s
      conversation_id
    else
      nil
    end
  end

  #API for Authentication
  def auth
    username = params[:username]
    password = params[:password]
    client_id = Rails.application.credentials.client_id
    client_secret_key = Rails.application.credentials.client_secret_key
  
    connection = Faraday.new(url: @base_url) do |faraday|
      faraday.request :url_encoded
      faraday.adapter Faraday.default_adapter
    end
  
    response = connection.post do |req|
      req.url '/oauth/token'
      req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
      req.body = "grant_type=password&username=#{username}&password=#{password}&client_id=#{client_id}&client_secret=#{client_secret_key}"
    end

    if response.success?
      access_token = JSON.parse(response.body)['access_token']
      render json: { access_token: }
    else
      error_message = response.body['error_description'] || 'Unknown error'
      render json: { error: error_message }, status: :unauthorized
    end
  end

  #Alternative API to fetch comments using twitter v2 endpoint
  def fetch_user_comments(conversation_id)
    comments = []

    conn = Faraday.new(url: @base_url) do |faraday|
      faraday.request :url_encoded
      faraday.adapter Faraday.default_adapter
    end

    # Fetch the initial tweet using conversation_id
    response = conn.get("/2/tweets/#{conversation_id}")
    initial_tweet = JSON.parse(response.body)
    # Get the user's screen_name
    screen_name = initial_tweet['data']['author_id']

    # Fetch replies using conversation_id
    response = conn.get("/2/tweets/search/recent?query=conversation_id:#{conversation_id}&tweet.fields=created_at,author_id,public_metrics,context_annotations&max_results=100")
    replies = JSON.parse(response.body)

    replies['data'].each do |reply|
      comments << reply['text']
    end
    comments
  end

  private
  def base_url
    @base_url = 'https://api.twitter.com'
  end
  #tokens and api keys
  def fetch_tokens
    @consumer_key = Rails.application.credentials.consumer_key
    @consumer_secret = Rails.application.credentials.consumer_secret
    @access_token = Rails.application.credentials.access_token
    @access_token_secret = Rails.application.credentials.access_token_secret
  end
end
