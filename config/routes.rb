Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  post '/auth', to: 'twitter#auth'
  post '/tweets', to: 'twitter#post_text'
  post '/videos', to: 'twitter#upload_video'
  post '/images', to: 'twitter#upload_image'
  get '/comments', to: 'twitter#fetch_comments'
end
